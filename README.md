# Blockchain with Golang

A simple blockchain go app to keep track of balance records


## Usage

```PowerShell
go run main.go
```

#### Get: get a blockchain with all the blocks in it
```HTTP
http://localhost:8080/ 
```
#### Post: send a new balance as json to be added in the blockchain
```HTTP
http://localhost:8080/  // Example: { "Balance": 10 }
```